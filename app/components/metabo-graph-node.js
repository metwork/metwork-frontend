import Component from '@ember/component';
import { computed } from '@ember/object';
import $ from 'jquery';
import { inject as service } from '@ember/service';

export default Component.extend({

    session: service('session'),

    nodeName: computed('nodeData.id', function () {
        let nodeData = this.get("nodeData")
        if (nodeData.nodeType === "reaction") {
            return nodeData.name
        }
        return null
    }),

    displayMolDraw: computed('nodeData.id', function () {
        let nodeData = this.get("nodeData")
        $("#chem-draw").empty().append('<canvas id="chem-draw-' + nodeData.id + '">mol</canvas>');
        switch (nodeData.nodeType) {
            case 'molecule':
                this.displayMolecule(nodeData)
                break
            case 'reaction':
                this.displayReaction(nodeData)
                break
        }
        return null
    }),

    cosineData: computed('nodeData.id', function () {
        let cosine = this.get("nodeData").cosine
        if (cosine) {
            let data = cosine.split(" | ").map(x => {
                let data = x.split(" : ")
                return { ionId: parseInt(data[0]), cosine: data[1] }
            });
                let fragMolExpIds = this.get("nodeData.fragMolExpIds")
                let ionId = data[0].ionId
                this.set("nodeData.fragMolExpId", fragMolExpIds[ionId])

            return data
        } else {
            return null
        }

    }),

    getSpectrumURL: computed("nodeData", function () {
        var nodeData = this.get("nodeData")
        const token = this.get('session.data.authenticated.token')
        const spectrumUpId = "mw0-" + nodeData.ionId
        const url = "http://ms_plot:8000/?spectrum_up_id=" + spectrumUpId


        var xhr = new XMLHttpRequest();

        xhr.open('GET', url);
        xhr.onreadystatechange = handler;
        xhr.setRequestHeader('X-Token', token);
        xhr.send();

        function handler() {
            if (this.readyState === this.DONE) {
                if (this.status === 200) {
                    var mediaSource = new MediaSource();
                    var data_url = URL.createObjectURL(mediaSource);
                    document.querySelector('#spectrum').src = data_url;
                }
            }
        }

        return url
    }),

    displayMolecule: function (node) {
        ChemDoodle.default_atoms_useJMOLColors = true;
        var viewACS = new ChemDoodle.TransformCanvas("chem-draw-" + node.id, 250, 250);
        viewACS.specs.bonds_width_2D = .6;
        viewACS.specs.bonds_saturationWidthAbs_2D = 2.6;
        viewACS.specs.bonds_hashSpacing_2D = 2.5;
        viewACS.specs.atoms_font_size_2D = 10;
        viewACS.specs.atoms_font_families_2D = ['Helvetica', 'Arial', 'sans-serif'];
        viewACS.specs.atoms_displayTerminalCarbonLabels_2D = true;
        var jsi = new ChemDoodle.io.JSONInterpreter();
        var moltarget = jsi.molFrom(node.molJSON)
        moltarget.scaleToAverageBondLength(14.4);
        viewACS.loadMolecule(moltarget);
    },



    displayReaction: function (node) {
        let canvasId = "chem-draw-" + node.id
        let dataJSON = node.reactJSON
        if (dataJSON) {
            this.noLabel = true
            var viewACS = new ChemDoodle.ViewerCanvas(
                canvasId,
                300,
                200);
            if (this.noLabel) {
                let line = {}
                dataJSON.s.map(function (shape) {
                    if (shape.t === 'Line') {
                        line = shape
                    }
                });
                dataJSON.s = [line]
            }
            var jsi = new ChemDoodle.io.JSONInterpreter();
            var target = jsi.contentFrom(dataJSON)
            if (!this.noLabel) {
                var l = 0
                target.shapes.map(function (shape) {
                    shape.label = l
                    shape.error = true
                    l += 1
                })
            }
            viewACS.loadContent(target.molecules, target.shapes);
        }

    },

    actions: {
        copySmiles() {
            var target = document.getElementById('node-smiles');
            target.select();
            document.execCommand("copy");
        },
    },
})