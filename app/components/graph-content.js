import Component from '@ember/component';
import { computed } from '@ember/object';


export default Component.extend({
    classNames: ['graph-content'],
    classNameBindings: ['displaySpectrum:with-ms-plot'],

    displaySpectrum: computed("nodeData", "nodeData.fragMolExpId", function () {
        if (this.nodeData) {
            return this.nodeData.fragMolExpId || this.nodeData.fragMolExpIds
        }
        return false
    }),

    displayClickOnNode: computed('nodeData','spinnerStatus', function() {
        return !this.nodeData && (this.spinnerStatus == "stop")
        }

    ),
    
});
