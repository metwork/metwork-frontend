import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({

    displayMolDraw: computed('nodeData.id', function() {
        let nodeData = this.get("nodeData")
        this.$("#chem-draw").empty().append('<canvas id="chem-draw-' + nodeData.id + '">mol</canvas>');
        this.displayMolecule(nodeData)
    }),

    displayMolecule: function(node) {
        ChemDoodle.default_atoms_useJMOLColors = true;
        var viewACS = new ChemDoodle.TransformCanvas("chem-draw-" + node.id, 250, 250);
        viewACS.specs.bonds_width_2D = .6;
        viewACS.specs.bonds_saturationWidthAbs_2D = 2.6;
        viewACS.specs.bonds_hashSpacing_2D = 2.5;
        viewACS.specs.atoms_font_size_2D = 10;
        viewACS.specs.atoms_font_families_2D = ['Helvetica', 'Arial', 'sans-serif'];
        viewACS.specs.atoms_displayTerminalCarbonLabels_2D = true;
        var jsi = new ChemDoodle.io.JSONInterpreter();
        var moltarget = jsi.molFrom(node.molJSON)
        moltarget.scaleToAverageBondLength(14.4);
        viewACS.loadMolecule(moltarget);
    },

    getSpectrumURL: computed("nodeData", function() {
        const nodeData = this.get("nodeData")
        const token = this.get('session.data.authenticated.token')
        const spectrumUpId =  nodeData.ionId
        let url = "http://ms_plot:8000/?spectrum_up_id=mw0-" + spectrumUpId
        const spectrumDownId = nodeData.bestAnnotation.fragMolId
        if (spectrumDownId && (spectrumDownId != spectrumUpId)) {
            url += "&spectrum_down_id=mw0-" + spectrumDownId
        }

        var xhr = new XMLHttpRequest();

        xhr.open('GET', url);
        xhr.onreadystatechange = handler;
        xhr.setRequestHeader('X-Token', token);
        xhr.send();
        
        function handler() {
        if (this.readyState === this.DONE) {
            if (this.status === 200) {
            var mediaSource = new MediaSource();
            var data_url = URL.createObjectURL(mediaSource);
            document.querySelector('#spectrum').src = data_url;
            }
        }
        }

        return url
    }),

    actions: {
        copySmiles() {
            var target = document.getElementById('node-smiles');
            target.select();
            document.execCommand("copy");
        },
    },

});
