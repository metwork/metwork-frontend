import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import ENV from '../config/environment'

export default Controller.extend({
  session: service('session'),
  currentUser: service('current-user'),
  apiStatus: service('api-status'),
  version: ENV.version,

  apiStatusUpdate: computed(
    'apiStatus.status.available', function () {
      if (!
        (this.get('apiStatus.status.available')
          || this.get('apiStatus.loading'))) {
        this.get('target').transitionTo('index');
      }
      return null
    }),

  userFirstLetter: computed('currentUser.user', function () {
    let currentUser = this.get('currentUser');
    if (currentUser.user) {
      return currentUser.user.get('username').charAt(0).toUpperCase();
    }
    return null
  }),

  actions: {
    invalidateSession() {
      this.get('session').invalidate();
    },
  },

});
